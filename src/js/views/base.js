export const elements = {
  searchForm: document.querySelector('.search'),
  searchInput: document.querySelector('.search__field'),
  searchResList: document.querySelector('.results__list'),
  searchRes: document.querySelector('.results'),
  searchResPages: document.querySelector('.results__pages'),
  recipe: document.querySelector('.recipe'),
  shopping: document.querySelector('.shopping__list'),
  shoppingc: document.querySelector('.empty__list'),
  shoppingadd: document.querySelector('.add__all'),
  shoppingaddto: document.querySelector('.addto__list'),
  shoppingingredient: document.querySelector('.input__ingredient'),
  likesMenu: document.querySelector('.likes__field'),
  likesList: document.querySelector('.likes__list'),
  inputCount: document.getElementById('input__count'),
  inputUnit: document.getElementById('input__unit'),
  inputIngredient: document.getElementById('input__ingredient')

};

export const elementStrings = {
  loader: 'loader'
}

export const renderLoader = parent => {
  const loader = `
  <div class ="${elementStrings.loader}">
  <svg>
    <use href="img/icons.svg#icon-cw"></use>
  </svg>
  </div>
  `;

  parent.insertAdjacentHTML('afterbegin', loader);
};

export const clearLoader = () => {
  const loader = document.querySelector(`.${elementStrings.loader}`);
  if (loader) loader.parentElement.removeChild(loader);
}
