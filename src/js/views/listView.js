import { elements } from "./base";

// import List from './models/List';


export const renderItem = item => {
  const markup = `
  <li class="shopping__item" data-itemid=${item.id}>
    <div class="shopping__count">
        <input type="number" value="${item.count}" step="${item.count}" class="shopping__count-value">
        <p>${item.unit}</p>
    </div>
    <p class="shopping__description">${item.ingredient}</p>
    <button class="shopping__delete btn-tiny">
        <svg>
            <use href="img/icons.svg#icon-circle-with-cross"></use>
        </svg>
    </button>
  </li>
  `;

  elements.shopping.insertAdjacentHTML('beforeend', markup);
};

export const deleteItem = id => {
  const item = document.querySelector(`[data-itemid="${id}"]`);

  if (item) item.parentElement.removeChild(item);
};

export const deleteItems = e => {
  const item = document.querySelector('.shopping__list');

  while (item.firstChild) {
    item.removeChild(item.firstChild);
  }

  // persistData();
};

export const clearFields = () => {
  document.getElementById('input__count').value = '';
  document.getElementById('input__unit').value = '';
  document.getElementById('input__ingredient').value = '';
}





