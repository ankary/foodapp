import uniqid from 'uniqid';
import * as listView from '../views/listView';
import { elements } from "../views/base";

export default class List {

  constructor() {
    this.items = [];
  }

  addItem(count, unit, ingredient) {
    const item = {
      id: uniqid(),
      count,
      unit,
      ingredient
    }
    this.items.push(item);
    this.persistData();
    return item;
  }

  deleteItem(id) {
    const index = this.items.findIndex(el => el.id === id);

    // [2,4,8] splice(1, 2) -> return [4, 8], original array is [2]
    // [2,4,8] slice(1, 2) -> return 4, original array is [2, 4, 8]
    this.items.splice(index, 1);

    this.persistData();
  }


  updateCount(id, newCount) {
    this.items.find(el => el.id === id).count = newCount;
  }

  addInputItem() {
    const item = {
      id: uniqid(),
      count: document.getElementById('input__count').value,
      unit: document.getElementById('input__unit').value,
      ingredient: document.getElementById('input__ingredient').value
    }


    this.items.push(item);
    this.persistData();
    return item;
  }

  persistData() {
    localStorage.setItem('ingredients', JSON.stringify(this.items));
  }

  readStorage() {
    const storage = JSON.parse(localStorage.getItem('ingredients'));

    // Restore the likes from the localStorage
    if(storage) this.items = storage;
  }

  deleteAll() {
    elements.shoppingc.addEventListener('click', e => {
        if (e.target.closest('.delete__all')) {

          // delete items from state
          state.list.items = [];

          // delete items from UI
          if(state.list.items){
            listView.deleteItems(state.list.items);
          }
        }
        this.persistData();
    });
  }
}


