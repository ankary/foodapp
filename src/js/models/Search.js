import axios from 'axios';
import { key, proxy } from '../config';

export default class Search {
  constructor(query) {
    this.query = query;
  }

  async getResults(query) {

    try {
      // const res = await axios(`${proxy}http://food2fork.com/api/search?key=${key}&q=${this.query}`);
      // const res = await axios(`https://forkify-api.herokuapp.com/api/v2/recipes?search/search=${this.query}&key=${key}`);
      // const res = await axios(`https://forkify-api.herokuapp.com/api/v2/recipes?search=${this.query}`);

      const res = await fetch(`https://forkify-api.herokuapp.com/api/v2/recipes?search=${this.query}`);
      const data = await res.json();

      this.result = data.data.recipes;
      console.log(this.result);
    } catch (error) {
      console.log(error);
    }
  }
}

